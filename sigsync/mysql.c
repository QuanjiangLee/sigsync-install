#include "sigsync.h"

int init_mysql_connect(MYSQL *mysql, char *ip_addr, char *user_name, char *password, char *db_name, char details[MAXTRANSLENGTH]){
		//连接之前，先用mysql_init函数初始化MYSQL句柄
	mysql_init(mysql);
		//使用mysql_real_connect函数连接数据库
	if(!mysql_real_connect(mysql, ip_addr, user_name, password, db_name, 0, NULL, 0)){
		//printf("ERROR: Connect Mysql Failed!\n");
		strcat(details, "ERROR: Connect Mysql Failed!");
		return -1;
	}
	return 0;
}

int exct_query(MYSQL *mysql, MYSQL_RES **res, int op_type, char *query, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name){

	init_mysql_connect(mysql, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details);

	if(!mysql_query(mysql,query)){
		if(op_type == MYSQL_SELECT){
			if(!(*res = mysql_store_result(mysql))){
				mysql_close(mysql);
				return -1;
			}
		}
	}else{
		mysql_close(mysql);
		return -1;
	}
	mysql_close(mysql);
	return 0;
}

int op_sql_file_list(MYSQL *mysql, MYSQL_RES **res, int op_type, struct file_struct *file, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH]){
	char buf[1000];
	bzero(buf, 1000);
	int query_result = 1;

	switch(op_type){
		case MYSQL_SELECT:
			//printf("SELECT\n");
			sprintf(buf, "Select checksum, actual_size from sig_file_list where path = \'%s\';", strlen(file->path)? file->path : "NULL");
			break;
		case MYSQL_DELETE:
			//printf("DELETE\n");
			sprintf(buf, "Delete from sig_file_list where path = \'%s\';", strlen(file->path)? file->path : "NULL");
			break;
		case MYSQL_UPDATE:
			//printf("UPDATE\n");
			sprintf(buf, "Update sig_file_list set type = %d, actual_size = %ld, size_on_disk = %ld, atime = %ld, mtime = %ld, ctime = %ld, checksum = \'%.*s\', mode = %d, remarks = \'%s\' where path = \'%s\';", 
				file->type? file->type : 0,
				file->actual_size? file->actual_size : 0, 
				file->size_on_disk? file->size_on_disk : 0, 
				file->atime? file->atime : 0, 
				file->mtime? file->mtime : 0, 
				file->ctime? file->ctime : 0, 
				strlen(file->checksum)? (int)strlen(file->checksum) : 4, 
				strlen(file->checksum)? file->checksum : "NULL", 
				file->mode? file->mode : 0, 
				strlen(file->remarks)? file->remarks : "No remarks", 
				strlen(file->path)? file->path : "NULL");
			break;
		case MYSQL_INSERT:
			//printf("INSERT\n");
			if(file->path){
				sprintf(buf, "Insert into sig_file_list (owner_name, path, path_hash, parent, name, type, actual_size, size_on_disk, atime, mtime, ctime, checksum, mode, remarks) values(\'%s\', \'%s\', \'%s\', %d, \'%s\', %d, %ld, %ld, %ld, %ld, %ld, \'%.*s\', %d, \'%s\');", 
					strlen(file->owner_name)? file->owner_name : "NULL", 
					strlen(file->path)? file->path:"No Path", 
					strlen(file->path_hash)? file->path_hash : "NULL", 
					file->parent,
					strlen(file->name)? file->name : "NULL", 
					file->type,
					file->actual_size? file->actual_size : 0, 
					file->size_on_disk? file->size_on_disk : 0, 
					file->atime? file->atime : 0, 
					file->mtime? file->mtime : 0, 
					file->ctime? file->ctime : 0, 
					strlen(file->checksum)? (int)strlen(file->checksum) : 4, 
					strlen(file->checksum)? file->checksum : "NULL",
					file->mode? file->mode : 0, 
					strlen(file->remarks)? file->remarks : "No remarks");
			}else{
				//printf("Insert path = NULL!\n");
				return -1;
			}
			break;
		default:
			return -1;
	}
	if((query_result = exct_query(mysql, res, op_type, buf, mysql_addr, mysql_user_name, mysql_password, mysql_db_name)) != 0)
		strcat(details, "Error excut query.");
	//mysql_close(mysql);
	return query_result;
}

int op_sql_log(MYSQL *mysql, struct operate_struct *operation, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char *details){
	
	char buf[1000];
	char *query;
	int query_result = 1;
	bzero(buf, 1000);
	strcat(operation->op_information, details);
	sprintf(buf, "Insert into sig_log (file_path, file_size, file_checksum, file_owner, file_events, file_mode, sender_ip, recver_ip, result, op_start_time, op_end_time, op_information, remark) values('%s', %ld, '%s', '%s', '%s', %d, '%s', '%s', '%s', %ld, %ld, '%s', '%s');", 
		strlen(operation->op_file.path)? operation->op_file.path: "No path",
		operation->op_file.actual_size? operation->op_file.actual_size : 0, 
		strlen(operation->op_file.checksum)? operation->op_file.checksum : "No checksum", 
		strlen(operation->op_file.owner_name)? operation->op_file.owner_name : "No owner name", 
		strlen(operation->op_events)? operation->op_events:"No event", 
		operation->op_file.mode? operation->op_file.mode : 0, 
		strlen(operation->op_sender_addr)? operation->op_sender_addr : "No sender", 
		strlen(operation->op_recver_addr)? operation->op_recver_addr : "No receiver", 
		strlen(operation->op_result)? operation->op_result : "Unkonw result", 
		operation->op_start_time? operation->op_start_time : 0, 
		operation->op_end_time? operation->op_end_time : 0, 
		strlen(operation->op_information)? operation->op_information : "No details", 
		strlen(operation->op_remarks)? operation->op_remarks : "No remarks");
	//if(errno){
	//	printf("error: %s\n", strerror(errno));
	//	return -1;
	//}
	//printf("SQL: %s\n", buf);
	if((query_result = exct_query(mysql, NULL, MYSQL_INSERT, buf, mysql_addr, mysql_user_name, mysql_password, mysql_db_name)) != 0)
		strcat(details, "Error excute log insert.");
	//mysql_close(mysql);
	return query_result;
}

