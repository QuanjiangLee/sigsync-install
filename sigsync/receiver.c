#include "sigsync.h"
#include "setjmp.h" 
//定义一个全局异常
jmp_buf Jump_Buffer;

#define MAXRECVEDBUFFER 100000

//宏定义try实现类似c++中的try-catch功能
#define try if(!setjmp(Jump_Buffer))  
#define catch else  
#define throw longjmp(Jump_Buffer,1)

char *fail = "FAIL";
char *success = "SUCCE";

//服务器端的socket地址
struct sockaddr_in	recver_addr;
//服务器端的socket族地址
struct sockaddr_in	sender_addr;
//描述操作的结构体
struct operate_struct operation;
//定义数据库连接句柄
static MYSQL mysql, *mysql_pointer;
//查询结果集，结构类型
static MYSQL_RES *res;
//包含字段信息的结构
static MYSQL_FIELD *fd;
//存放一行查询结果的字符串数组
static MYSQL_ROW row;
//文件类型
struct stat st;
//存放处理后数据的缓存队列
char ***trans_data;
//存放处理后数据长度的缓存队列
int **trans_data_length;
//存放原始数据的缓存队列
char recved[MAXRECVEDBUFFER][MAXTRANSLENGTH];
//存放原始数据长度的缓存队列
int recved_length[MAXRECVEDBUFFER];
//存放操作数据包长度的队列_第一维表示总长度，第二维表示接收到的总长度
int operationDataLength[MAXOPERATE][2];
//监控原始数据缓存队列的锁
pthread_mutex_t recved_mut;
//当前缓存数据的位置，头
int recved_pre_index;
//原始缓存数据尾
int recved_tail_index;
//原始缓存当前准备存放数据的最前位置
//int putIndex;
//监控原始缓存数据尾的值的锁
pthread_mutex_t tail_mut;
//监控原始缓存数据头的值的锁
pthread_mutex_t head_mut;
//距离
int distence;
//期望的操作编号
int expectOperate;
//监控距离的锁
pthread_mutex_t distence_mut;
//监控处理后数据队列的锁
pthread_mutex_t trans_data_mut;
//监控数据库的锁
pthread_mutex_t mysql_mut;

//用于处理事件的线程池
pthread_t operater_thread[MAXOPERATE];

int mysql_Operations[] = {MYSQL_INSERT, MYSQL_UPDATE, MYSQL_DELETE};

int old_version;
//int sender_addr_length;

void free_space(int op_index){
	if(trans_data[op_index] != NULL){
		int p;
		for(p = 0; p < operationDataLength[op_index][0]; p++){
			if(trans_data[op_index][p] != NULL){
				free(trans_data[op_index][p]);
				trans_data[op_index][p] = NULL;
			}
			//if(trans_data_length[op_index][p] != NULL){
				//free(trans_data_length[op_index][p]);
				//trans_data_length[op_index][p] = NULL;
			//}
		}
		if(trans_data[op_index] != NULL){
			free(trans_data[op_index]);
			trans_data[op_index] = NULL;
		}
		if(trans_data_length[op_index] != NULL){
			free(trans_data_length[op_index]);
			trans_data_length[op_index] = NULL;
		}
	}
}


int error_exit(struct operate_struct *operation, MYSQL *mysql, char *details, char *error_info, char *result){
	strcat(details, error_info);
	if(operation->op_end_time == 0)
		time(&operation->op_end_time);
	strcpy(operation->op_result, result);
	return process_error(mysql, operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details);
}

void *operator(void *index){

	int indexOfOperate = *((int*)index);
	free((int *)index);

	char content[MAXTRANSLENGTH];
	bzero(content, MAXTRANSLENGTH);
	
	struct operate_struct operation;
	//sleep(3);
	printf("\n%dth operation is waitting for data...\n", indexOfOperate);
	int timer = 0;
	int oldNumData = 0;
	while(operationDataLength[indexOfOperate][1] < operationDataLength[indexOfOperate][0]){
		//当数据不齐全时. [0]表示总数;[1]表示当前收到的数目。若连续一段时间没有新数据达到，认为操作丢失信息，取消操作
		if(operationDataLength[indexOfOperate][1] == oldNumData){
			//若[1]与oldNmData相等，认为当前没有新数据到达
			if(timer < 5){
				timer++;
				printf("timer = %d, current num = %d, total = %d\n", timer, operationDataLength[indexOfOperate][1], operationDataLength[indexOfOperate][0]);
			}else{
				//连续没有收到数据超过一段时间
				printf("%dth operation's(%.*s) data number is not correct.", indexOfOperate, trans_data_length[indexOfOperate][0],trans_data[indexOfOperate][0]);
				printf("start to free %dth operation's space\n", indexOfOperate);
					//尝试获取操作信息，记录到数据库中
					try{
						pthread_mutex_lock(&trans_data_mut);
					}catch{
						puts("-----------something in pthread_mutex_lock() error!!----------");
						pthread_mutex_lock(&trans_data_mut);
					}
				if(trans_data[indexOfOperate][0])
					strncopy(content, trans_data[indexOfOperate][0], trans_data_length[indexOfOperate][0]);
				pthread_mutex_unlock(&trans_data_mut);

				if(strlen(content) && !process_massege(&operation, content, details)){
					if(operation.op_start_time == 0)
						time(&operation.op_start_time);
					strcat(operation.op_result, fail);
					strcat(details, "Error when try to recv data of operation.");
					if(op_sql_log(&mysql, &operation,  mysql_addr, mysql_user_name, mysql_password, mysql_db_name,details) != 0)
						error_exit(&operation, &mysql, details, "Error when try to recv data of operation!\n", fail);
				}
					//释放操作空间
					try{
						pthread_mutex_lock(&trans_data_mut);
					}catch{
						puts("-----------something in pthread_mutex_lock() error!!----------");
						pthread_mutex_lock(&trans_data_mut);
					}
				free_space(indexOfOperate);
				pthread_mutex_unlock(&trans_data_mut);
				printf("operate %d end\n", indexOfOperate);
				return (void *)NULL;
			}
		}else{
			printf("%dth operation's current data's number = %d, oldNum = %d\n", indexOfOperate, operationDataLength[indexOfOperate][1], oldNumData);
			oldNumData = operationDataLength[indexOfOperate][1];
			timer = 0;
		}
			//这里可以修改为合适的值
		usleep(500);
	}
	printf("%dth operation's data is correct, start to process.\n", indexOfOperate);
		//尝试获取操作信息
		
	//异常处理
	try{
		pthread_mutex_lock(&trans_data_mut);
	}catch{
		puts("-----------something in pthread_mutex_lock() error!!----------");
		pthread_mutex_lock(&trans_data_mut);
	}
	
	if(trans_data[indexOfOperate] && trans_data[indexOfOperate][0] && trans_data_length[indexOfOperate] && trans_data_length[indexOfOperate][0])
		strncopy(content, trans_data[indexOfOperate][0], trans_data_length[indexOfOperate][0]);
	pthread_mutex_unlock(&trans_data_mut);

	char informations[200];
	int isFileExist = 0;
	int mysql_op_Type = 0;
	int operate_State = 0;

	if(strlen(content) && !process_massege(&operation, content, details)){
		if(operation.op_start_time == 0)
			time(&operation.op_start_time);
		if((strstr(operation.op_events, events[CREATE]) != NULL) || (strstr(operation.op_events, events[CLOSE_WRITE]) != NULL) || (strstr(operation.op_events, events[MOVED_TO]) != NULL) || (strstr(operation.op_events, events[MODIFY]) != NULL) || (strstr(operation.op_events, events[ATTRIB]) != NULL)){
			printf("%dth operation: time: %ld, CREATE or CLOSE_WRITE or MOVED_TO or MODIFY or Attrib, start to query in mysql ", indexOfOperate, operation.op_start_time);
			//select from mysql
			try{
				pthread_mutex_lock(&mysql_mut);
			}catch{
				puts("-----------something in pthread_mutex_lock() error!!----------");
				pthread_mutex_lock(&mysql_mut);
			}
			if(!isGenerateFile(&operation, &mysql, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details, &isFileExist) || (access(operation.op_file.path, 0) != 0)){
				pthread_mutex_unlock(&mysql_mut);
				printf("mysql end, file does not exist.");
				//not exist in mysql
				if(strstr(operation.op_events, events[ISDIR]) != NULL){
					//if dir
					printf("dir: %s ----------", operation.op_file.path);
					if(access(operation.op_file.path, 0) != 0){
						//dir not exist, mkdir
						if(mkdir(operation.op_file.path, 0755)){
							//mkdir failed
							//time(&operation.op_end_time);
							operate_State = 0;
						}else{
							//if success
							operate_State = 1;
						}
					}else{
						operate_State = 1;
					}
				}else{
					//if file
					printf("file: %s ---------", operation.op_file.path);
					if(!create_new_file(&operation, trans_data, trans_data_length, indexOfOperate, 1, operationDataLength[indexOfOperate][0], old_version, details)){
						operate_State = 1;
					}else{
						operate_State = 0;
					}
				}
			}else{
				pthread_mutex_unlock(&mysql_mut);
				//same with operate and file exist
				printf("%dth operation: mysql end, file exists.", indexOfOperate);
				operate_State = 1;
				strcat(details, "same file, do not change.");
			}
		}else if((strstr(operation.op_events, events[DELETE]) != NULL) || (strstr(operation.op_events, events[MOVED_FROM]) != NULL)){
			mysql_op_Type = 2;
			printf("%dth operation: time: %ld, DELETE or MOVED_FROM %s -----------", indexOfOperate, operation.op_start_time, operation.op_file.path);
			int rm_result = 0;
			if(strstr(operation.op_events, events[ISDIR]) != NULL)
				rm_result = rmdir(operation.op_file.path);
			else
				rm_result = remove(operation.op_file.path);
			if(rm_result == 0){
				operate_State = 1;
			}else{
				operate_State = 0;
			}
		}else{
			printf("%dth operation: Events received can not be processed!\n", indexOfOperate);
			operate_State = 1;
		}

			try{
				pthread_mutex_lock(&mysql_mut);
			}catch{
				puts("-----------something in pthread_mutex_lock() error!!----------");
				pthread_mutex_lock(&mysql_mut);
			}
		if(operate_State){
			if(op_sql_file_list(&mysql, NULL, mysql_Operations[mysql_op_Type + isFileExist], &operation.op_file, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
				error_exit(&operation, &mysql, details, "Error excute mysql operation with table op_sql_file_list!", fail);
			}else{
				time(&operation.op_end_time);
				strcat(operation.op_result, success);
				if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name,details) != 0)
					error_exit(&operation, &mysql, details, "Error when try to insert log operation!\n", success);
			}
		}else{
			time(&operation.op_end_time);
			strcat(operation.op_result, fail);
			if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name,details) != 0)
				error_exit(&operation, &mysql, details, "Error when try to insert log operation!\n", fail);
		}

		pthread_mutex_unlock(&mysql_mut);
		
	}else{
		printf("\n%dth operation's operate information is null or failed to get the informations\n", (int)strlen(content));
	}
	printf("start to free %d...\n", indexOfOperate);
	try{
		pthread_mutex_lock(&trans_data_mut);
	}catch{
		puts("-----------something in pthread_mutex_lock() error!!----------");
		pthread_mutex_lock(&trans_data_mut);
	}
	free_space(indexOfOperate);
	pthread_mutex_unlock(&trans_data_mut);
	printf("operate %d end\n", indexOfOperate);
}

void *processer(void *recv){
	
	int recv_len;
	int indexOfOperate;
	int dataNum;
	int number;
	int operateNum;
	int contentLength;

	//int startFlag = -1;

	char *operateNumString;
	char *dataNumString;
	char *checksum;
	char *numberString;
	char *content;
	
	char recv_content[MAXTRANSLENGTH];
	
	//int wait = 0;
	while(1){
		bzero(recv_content, MAXTRANSLENGTH);
	    try{
		        pthread_mutex_lock(&tail_mut);
			}catch{
				puts("-----------something in pthread_mutex_lock() error!!----------");
				pthread_mutex_lock(&tail_mut);
			}
	    try{
				pthread_mutex_lock(&head_mut);
			}catch{
				puts("-----------something in pthread_mutex_lock() error!!----------");
				pthread_mutex_lock(&head_mut);
			}		
		if(recved_tail_index < recved_pre_index){
				//获取原始数据末尾位置
			int tail = recved_tail_index++;
			int tailIndex = tail % MAXRECVEDBUFFER;
			pthread_mutex_unlock(&tail_mut);
			pthread_mutex_unlock(&head_mut);
			//printf("deal info, ");
				//从数据缓冲区拿数据
			try{
				pthread_mutex_lock(&recved_mut);
			}catch{
				puts("-----------something in pthread_mutex_lock() error!!----------");
				pthread_mutex_lock(&recved_mut);
			}		
			recv_len = recved_length[tailIndex];
			if(recv_len == 0){
				printf("length = 0\n");
				bzero(recved[tailIndex], recv_len);
				pthread_mutex_unlock(&recved_mut);
			}else{
				strncopy(recv_content, recved[tailIndex], recv_len);
					//清空原始数据缓冲区内的内容
				bzero(recved[tailIndex], recv_len);
				recved_length[tailIndex] = 0;
				pthread_mutex_unlock(&recved_mut);

				//printf("%s\n", recv_content);
					//开始处理收到的数据
						//提取前缀信息：包括操作编号、操作数据总数、数据的完整性验证信息、数据编号。
					//获取操作编号(operateNum)
				operateNumString = recv_content;
				if(strlen(operateNumString)){
					dataNumString = strchr(operateNumString, SPLITCHAR);
					if(dataNumString != NULL){
						*dataNumString++ = '\0';
					}
				}
				operateNum = atoi(operateNumString);
				int operateNumLength = strlen(operateNumString);
					//获取操作数据总数（dataNum）（不包括操作信息）
				if(dataNumString && strlen(dataNumString)){
					checksum = strchr(dataNumString, SPLITCHAR);
					if(checksum != NULL){
						*checksum++ = '\0';
						*(checksum + 32) = '\0';
					}else{
						printf("information error in try to split dataNumString: %.*s\n", recv_len - operateNumLength, dataNumString);
						continue;
					}
				}
				dataNum = atoi(dataNumString);
				int dataNumLength = strlen(dataNumString);
					//获取数据完整性验证信息（checksum）和数据编号（number）
				if(checksum && strlen(checksum)){
					if(operateNumLength + dataNumLength + 33 < recv_len){
						numberString = checksum + 33;
						//*(numberString -1) = '\0';
						content = strchr(numberString, SPLITCHAR);
						if(content && strlen(content)){
							*content++ = '\0';
						}else{
							printf("information error in trying to split numberString: %.*s\n", recv_len - operateNumLength - dataNumLength - 32, numberString);
							continue;
						}
					}else{
						printf("information error in try to split checksum: %.*s\n", recv_len - operateNumLength - dataNumLength, checksum);
					}
				}
				number = atoi(numberString);
				int numberLength = strlen(numberString);
					//推出数据部分的长度
				contentLength = recv_len - operateNumLength - dataNumLength - 32 - numberLength - 4;
				if(!contentLength)
					continue;
				try{
					pthread_mutex_lock(&distence_mut);
				}catch{
					puts("-----------something in pthread_mutex_lock() error!!----------");
					pthread_mutex_lock(&distence_mut);
				}		
					//计算当前发送方与接收方之间的操作个数差值
				if(expectOperate == 0)
					distence = operateNum;
				if(operateNum == 0){
					//if(contentLength){
					//printf("operate = 0, so number = %d, content = %s,checksum=%s\n", number, content, checksum);
						expectOperate = 0;
						distence = 0;/*
					}else{
						printf("information error of contentLength, tail %d, head %d, recv_len = %d, %ld->%ld->%ld->%ld\n", recved_tail_index, recved_pre_index, recv_len, (long int)operateNumString, (long int)dataNumString, (long int)checksum, (long int)numberString );
						//recved_tail_index--;
						pthread_mutex_unlock(&distence_mut);
						continue;
					}*/
				}
				pthread_mutex_unlock(&distence_mut);
					//获取当前操作的实际编号
				indexOfOperate = (operateNum - distence) % MAXOPERATE;
				//printf("params finish, %d, %d, ope= %d, dis = %d, dataNum = %d\n", indexOfOperate, number, operateNum, distence, dataNum);
				try{
					pthread_mutex_lock(&trans_data_mut);
				}catch{
					puts("-----------something in pthread_mutex_lock() error!!----------");
					pthread_mutex_lock(&trans_data_mut);
				}
				//printf("operateNum = %d; (expectOperate + distence) = %d\n", operateNum, (expectOperate + distence));
				int mallocFlag = 1;//1表示分配成功;0表示失败
				if(operateNum == (expectOperate + distence) && !trans_data[indexOfOperate]){
					//当数据满足期望的操作值，而且队列中该位置的值为空,认为是新操作，需要创建新的操作(向前，即创建后不再创建同一位置)
						//分配空间
					//printf("malloc space for operations\n");
					trans_data[indexOfOperate] = (char **)malloc(sizeof(char *) * (dataNum + 1));
					if(trans_data[indexOfOperate] == NULL){
						printf("there is no more space for new operation.\n");
						//pthread_mutex_unlock(&trans_data_mut);
						//continue;
						mallocFlag = 0;
					}else{
						bzero(trans_data[indexOfOperate], (dataNum + 1) * sizeof(char *));
						trans_data_length[indexOfOperate] = (int *)malloc(sizeof(int) * (dataNum + 1));
						if(trans_data_length[indexOfOperate] == NULL){
							printf("there is no more space for new operation's length of packets.\n");
							free(trans_data[indexOfOperate]);
							//pthread_mutex_unlock(&trans_data_mut);
							//continue;
							mallocFlag = 0;
						}else{
							bzero(trans_data_length[indexOfOperate], sizeof(int) * (dataNum + 1));
							expectOperate++;
							//指定新操作的数据包长度
							operationDataLength[indexOfOperate][0] = (dataNum + 1);
							//初始化指定操作接收到的数据包数
							operationDataLength[indexOfOperate][1] = 0;
						}
					}
				}else{
					if(!number){
						printf("operateNum = %d ---- expectOperate + distence = %d, trans_data[%d] = %ld", operateNum, expectOperate + distence, indexOfOperate, (long int)trans_data[indexOfOperate]);
					}
				}
				pthread_mutex_unlock(&trans_data_mut);
				if(!mallocFlag)
					continue;
				//printf("start to process\n");
				//printf("start to add info into %d, %d, length=%d \n", indexOfOperate, number, contentLength);
				//printf("operateNum = %d; (expectOperate + distence - 5) = %d\n", operateNum, (expectOperate + distence - 5));
					//开始处理信息
				if(operateNum >= (expectOperate + distence - 5)){
				try{
					pthread_mutex_lock(&trans_data_mut);
				}catch{
					puts("-----------something in pthread_mutex_lock() error!!----------");
					pthread_mutex_lock(&trans_data_mut);
				}
					if(trans_data[indexOfOperate] && !trans_data[indexOfOperate][number]){
						char hash[BASE64CHECKSUM];
						if(get_md5_checksum(content, hash, details, contentLength)){
							printf("error when try to get the checksum of content from buf_operation.\n");
							//continue;
						}else{
							//printf("checksum, \n");
							if(!strcmp(hash, checksum)){
								//收到的数据正确
								//printf("right massege\n");
								if(trans_data[indexOfOperate]){
									//printf("malloc for massege\n");
									if(!number)
										printf("recved operation infomation of operate %d\n", indexOfOperate);
									if(trans_data[indexOfOperate]  != NULL && trans_data[indexOfOperate][number] == NULL){
										trans_data[indexOfOperate][number] = (char *)malloc(sizeof(char) * contentLength);
										//printf("malloc success!\n");
										if(trans_data[indexOfOperate]!= NULL && trans_data[indexOfOperate][number] != NULL){
											bzero(trans_data[indexOfOperate][number], sizeof(char) * contentLength);
											if(content && contentLength > 0){
												//添加新信息
												//printf("add data info to %d, %d\n", indexOfOperate, number);
												strncopy(trans_data[indexOfOperate][number], content, contentLength);
												trans_data_length[indexOfOperate][number] = contentLength;
												operationDataLength[indexOfOperate][1]++;
											}
											//printf("add data, \n");
											if(!number){
												//若接收到的是操作信息，唤起一个线程处理该操作
												int *preIndex = (int *)malloc(sizeof(int));
												*preIndex = indexOfOperate;
												//printf("try to fork a thread\n");
												pthread_create(&operater_thread[indexOfOperate], NULL, operator, preIndex);
											}
										}else{
											printf("there is no more space for operate #%d's data.\n", operateNum);
											//pthread_mutex_unlock(&trans_data_mut);
											//continue;
										}
									}else{
										//this means the data is already exists in buffer.
										if(!number && trans_data[indexOfOperate]!= NULL && trans_data[indexOfOperate][number] != NULL)
											printf("operation's info is already exists in buffer\n");
										if(trans_data[indexOfOperate] == NULL)
				                    					printf("operation has been freed\n");
									}
								}
							}else{
								printf("Massege Wrong!->%.*s<-%s->%d compared with ->%.*s<-%s->%d\n", contentLength, content, hash, contentLength, recv_len, recved[tail % MAXRECVEDBUFFER], checksum, recv_len);
							}
						}
					}else{
						//否则，此数据包所属的操作已经完结，丢弃
						printf("Operation end! Droped!\n");
					}
					pthread_mutex_unlock(&trans_data_mut);
				}else{
					printf("Operate number out of range!\n");
				}
				//printf("add info finished!\n");
				//printf("%d\n", tail);
			}
		}else{
			pthread_mutex_unlock(&tail_mut);
			pthread_mutex_unlock(&head_mut);
		}
	}
}

void *receiver_function(){
	char buffer[MAXTRANSLENGTH];
	bzero(buffer, MAXTRANSLENGTH);
	int recv_len = 0;
	int index = 0;
	int sender_addr_length = sizeof(sender_addr);
	while((recv_len = recvfrom(sock_id, buffer, MAXTRANSLENGTH, 0, (struct sockaddr *)&sender_addr, &sender_addr_length)) > 0){
		try{
			pthread_mutex_lock(&head_mut);
		}catch{
			puts("-----------something in pthread_mutex_lock() error!!----------");
			pthread_mutex_lock(&head_mut);
		}
		
			//获取当前原始数据缓存区的前置位
		index = recved_pre_index % MAXRECVEDBUFFER;
		pthread_mutex_unlock(&head_mut);
			//将数据和长度保存
		strncopy(recved[index], buffer, recv_len);
		recved_length[index] = recv_len;
			//向前移动一位
		try{
			pthread_mutex_lock(&head_mut);
		}catch{
			puts("-----------something in pthread_mutex_lock() error!!----------");
			pthread_mutex_lock(&head_mut);
		}
		recved_pre_index++;
		pthread_mutex_unlock(&head_mut);
			//清空buffer
		bzero(buffer, recv_len);
	}
}

int receiver_main(){
	printf("initializing...");
	char tempInfo[MAXTRANSLENGTH];
	//initial some params
	GetProfileString("./sigsync.conf", "mysql", "addr", mysql_addr);
	GetProfileString("./sigsync.conf", "mysql", "name", mysql_user_name);
	GetProfileString("./sigsync.conf", "mysql", "password", mysql_password);
	GetProfileString("./sigsync.conf", "mysql", "db_name", mysql_db_name);

	//GetProfileString("./sigsync.conf", "server", "ip", recver_ip);
	GetProfileString("./sigsync.conf", "server", "port", recver_port_string);
	//GetProfileString("./sigsync.conf", "server", "max_connect", max_connect);
	GetProfileString("./sigsync.conf", "server", "receiverNum", receiverNumString);
	GetProfileString("./sigsync.conf", "server", "processerNum", processerNumString);

	GetProfileString("./sigsync.conf", "server", "old_version", tempInfo);
	old_version = atoi(tempInfo);

	//获得receiver的socket
	//printf("\nTry to bind socket...");
	if((sock_id = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
		printf("Create socket failed!\n");
		return -1;
	}
		//fill the server sockaddr_in struct
	bzero(&recver_addr, sizeof(recver_addr));
	recver_addr.sin_family = AF_INET;
	recver_addr.sin_port = htons(atoi(recver_port_string));
	recver_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		//bind the socket
	if(bind(sock_id, (struct sockaddr *)&recver_addr, sizeof(recver_addr)) < 0){
		printf("Bind socket failed!\n");
		return -1;
	}
	printf("\nTry to set parameters of socket...");
	int recv_size;
	socklen_t optlen;
	optlen = sizeof(recv_size);
	getsockopt(sock_id, SOL_SOCKET, SO_RCVBUF, &recv_size, &optlen);
	//printf("recv_size: %d\n", recv_size);
	recv_size = 67108864 * 3;
	setsockopt(sock_id, SOL_SOCKET, SO_RCVBUF, &recv_size, optlen);
	getsockopt(sock_id, SOL_SOCKET, SO_RCVBUF, &recv_size, &optlen);
	//printf("recv_size: %d\n", recv_size);
	memset(&sender_addr, 0, sizeof(sender_addr));

	//pthread_t receiver[100];
	pthread_mutex_init(&recved_mut, NULL);
	pthread_mutex_init(&tail_mut, NULL);
	pthread_mutex_init(&head_mut, NULL);
	pthread_mutex_init(&mysql_mut, NULL);
	pthread_mutex_init(&distence_mut, NULL);
	pthread_mutex_init(&trans_data_mut, NULL);
	recved_pre_index = 0;
	recved_tail_index = 0;
	distence = 0;
	expectOperate = 0;
	//putIndex = 0;
	printf("\nInitialize other parameters...");
	trans_data = (char ***)malloc(sizeof(char *) * MAXOPERATE);
	bzero(trans_data, sizeof(char *) * MAXOPERATE);
	trans_data_length = (int **)malloc(sizeof(int *) * MAXOPERATE);
	bzero(trans_data_length, sizeof(int *) * MAXOPERATE);
	
	int processerNum = atoi(processerNumString);
	int receiverNum = atoi(receiverNumString);

	//接收线程
	pthread_t receiver_thread[100];
	//处理线程
	pthread_t processer_thread[100];
	int pthread_num = 0;

	//printf("\nStart to create threads...");
	for(; pthread_num < receiverNum; pthread_num++)
		pthread_create(&receiver_thread[pthread_num], NULL, receiver_function, NULL);
	for(pthread_num = 0; pthread_num < 1 && pthread_num < 100; pthread_num++)
		pthread_create(&processer_thread[pthread_num], NULL, processer, NULL);
	printf("\nSuccess! Wait for data...\n");

	//int old = 0;
	while(1){
		sleep(1);
		for(pthread_num = 0; pthread_num < processerNum && pthread_num < 100; pthread_num++){
			//printf("What's this!?");
			sleep(1);
			if(pthread_kill(processer_thread[pthread_num],0) != 0){
				pthread_create(&processer_thread[pthread_num], NULL, processer, NULL);
			}
		}
	}
}

