#define CREATE 		0
#define CLOSE_WRITE 	1
#define MOVED_FROM 	2
#define MOVED_TO	3
#define ISDIR		4
#define DELETE		5
#define MODIFY		6
#define ATTRIB		7

static char *events[] = {
	"CREATE",
	"CLOSE_WRITE",
	"MOVED_FROM",
	"MOVED_TO",
	"ISDIR",
	"DELETE",
	"MODIFY",
	"ATTRIB"
};
