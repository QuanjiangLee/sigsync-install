#define MYSQL_SELECT 0
#define MYSQL_DELETE 1
#define MYSQL_UPDATE 2
#define MYSQL_INSERT 3

#ifndef MAXTRANSLENGTH
#define MAXTRANSLENGTH 4000
#endif

int init_mysql_connect(MYSQL *mysql, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char error_info[MAXTRANSLENGTH]);
int exct_query(MYSQL *mysql, MYSQL_RES **res, int op_type, char *query, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name);
int op_sql_file_list(MYSQL *mysql, MYSQL_RES **res, int op_type, struct file_struct *file, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH]);
int op_sql_log(MYSQL *mysql, struct operate_struct *operation, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char *details);
