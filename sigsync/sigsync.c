#include "sigsync.h"
#include "main.h"

//Socket address of Server
struct sockaddr_in	recver_addr;
//the structure to describe operation
struct operate_struct operation;
//database connecetion handle
static MYSQL mysql, *mysql_pointer;
//query result structre
static MYSQL_RES *res;
//structure include field information
static MYSQL_FIELD *fd;
//to store a row of result
static MYSQL_ROW row;
//structure of File
struct stat st;
//type of operation
int inotifyType;
//the number of operations that were not sent to server
int noSendNum = 0;
//for event_List
pthread_mutex_t events_lock;

int error_to_exit(struct operate_struct *operation, MYSQL *mysql, char *details, char *error_info, char *result){
	//this method is called when there is an error which leads to the end of current operation procedure
		//add error information into error field
	strcat(details, error_info);
		//set the end time of operation
	if(operation->op_end_time == 0)
		time(&operation->op_end_time);
		//set operation's result
	strcpy(operation->op_result, result);
		//call process_error to deal with mysql operations
	return process_error(mysql, operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details);
}

void *process_event(){
	//This method is called to actually do the procession of operations
	char temp[GENERATELENGTH];
	char *temp_pointer;
	
	//initialization of operation
	bzero(&operation, sizeof(operation));
	time(&operation.op_start_time);
	bzero(details, MAXTRANSLENGTH);

	//process parameters
		//generate the receiver address
	sprintf(temp, "%s::%s", recver_ip, recver_port_string);
	strcpy(operation.op_recver_addr, temp);
	bzero(temp, GENERATELENGTH);
		//process op_information, op_events, op_file.path, owner_name, op_file.name, parent, type,
	strcpy(operation.op_information, strlen(argv[EVENTTIMEINDEX])? argv[EVENTTIMEINDEX]:"No operate time information.");
	strcpy(operation.op_events, argv[EVENTSINDEX]);
	strcpy(operation.op_file.path, argv[FILENAMEINDEX]);
	operation.op_file.path[strlen(operation.op_file.path)] = '\0';
	temp_pointer = get_owner_name(operation.op_file.path, details);
	//get the name of fold of this file
	if(temp_pointer != NULL){
		strcpy(operation.op_file.owner_name, temp_pointer);
	}
	//get the name of this file
	temp_pointer = get_name(operation.op_file.path, details);
	if(temp_pointer != NULL){
		strcpy(operation.op_file.name, temp_pointer);
	}
	operation.op_file.parent = get_parent(&mysql,operation.op_file.path, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details);
	operation.op_file.type = get_file_type(&mysql, operation.op_file.path, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details);
	if(get_md5_checksum(operation.op_file.path, operation.op_file.path_hash, details, strlen(operation.op_file.path))){
		noSendNum++;
		error_to_exit(&operation, &mysql, details, ";Error try to get checksum.", RESULT_FAIL);
		pthread_exit(NULL);
	}
	
	//initiate file struct stat
	if(lstat(operation.op_file.path, &st) != 0){
		//initiate file failed
		//if the operation is delete or moved_from, then ok; 
		//else, the current operation process failed
		if((strstr(operation.op_events, events[DELETE]) == NULL) 
			&& (strstr(operation.op_events, events[MOVED_FROM]) == NULL)){
			noSendNum++;
			error_to_exit(&operation, &mysql, details, ";Error when try to initiate file struct stat!", RESULT_FAIL);
			pthread_exit(NULL);
		}
	}else{
		//test the file's type, only dir and reg can be processed
		if(!S_ISDIR(st.st_mode) && !S_ISREG(st.st_mode)){
			noSendNum++;
			error_to_exit(&operation, &mysql, details, ";Not Reg file, neither a dir, do not process", RESULT_FAIL);
			printf("Not reg file, neither dir, do not process!\n");
			pthread_exit(NULL);
		}
		//initiate file's actual_size, size_on_disk, atime, mtime, ctime, checksum(based on MD5), mode
		operation.op_file.actual_size = st.st_size;
		operation.op_file.size_on_disk = st.st_blksize * st.st_blocks;
		operation.op_file.atime = st.st_atime;
		operation.op_file.mtime = st.st_mtime;
		operation.op_file.ctime = st.st_ctime;
		bzero(operation.op_file.checksum, BASE64CHECKSUM);
		operation.op_file.mode = st.st_mode;
	}
	
	//start to process current operation
	if((strstr(operation.op_events, events[CLOSE_WRITE]) != NULL) || (strstr(operation.op_events, events[MOVED_TO]) != NULL)
		 || (strstr(operation.op_events, events[MODIFY]) != NULL) || (strstr(operation.op_events, events[CREATE]) != NULL) 
		 || (strstr(operation.op_events, events[ATTRIB]) != NULL)){
		printf("time: %ld, CREATE or CLOSE_WRITE or MOVED_TO or MODIFY or Attrib ",  operation.op_start_time);
		if(strstr(operation.op_events, events[ISDIR]) != NULL){
			//if the object is a dir
			char message[MAXTRANSLENGTH];
				//generate message to be sent to server
			gen_send_massege(message, &operation, process_index - noSendNum, 0);
			if(sendto(sock_id, message, strlen(message), 0, (struct sockaddr *)&recver_addr, sizeof(recver_addr)) < 0){
				//send message failed.
				noSendNum++;
				error_to_exit(&operation, &mysql, details, "error when try to send CREATE or CLOSE_WRITE or MOVED_TO or MODIFY or Attrib operate of dir to Server.", RESULT_FAIL);
				printf("dir: %s ----------Failed!\n", operation.op_file.path);
				pthread_exit(NULL);
			}
			if(op_sql_file_list(&mysql, NULL, MYSQL_INSERT, &operation.op_file, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
				//sent message success but an error has occured when try to write information into mysql
				error_to_exit(&operation, &mysql, details, "error when try to insert the information of file!", RESULT_FAIL);
				printf("dir: %s ----------Success! but mysql operation failed!\n", operation.op_file.path);
				pthread_exit(NULL);
			}
			//write relative information into log file????
			strcat(details, "CREATE or CLOSE_WRITE or MOVED_TO or MODIFY or Attrib dir.");
			printf("dir: %s ----------Success!\n", operation.op_file.path);
			//set the result and end time of this operation
			strcpy(operation.op_result, RESULT_SUCCESS);
			time(&operation.op_end_time);

			if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
				//write into log(mysql) failed
					//add operation info.
				strcat(details, "Error when try to insert CREATE or CLOSE_WRITE or MOVED_TO or MODIFY or Attrib dir operation information to log.");
					//add mysql error info.
				strcat(details, mysql_error(&mysql));
				//printf("\n%s\n", details);
				process_error(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details);
				pthread_exit(NULL);
			}
		}else{
			//if the object is a file
			long int start, end;
			printf("file: %s, size: %ld\n", operation.op_file.path, st.st_size);
			
			//calculate the checksum(based on MD5) of file
			if(get_file_checksum(operation.op_file.path, operation.op_file.checksum, details, operation.op_file.actual_size)){
				noSendNum++;
				error_to_exit(&operation, &mysql, details, ";can not open when calculate md5 value!process failed!", RESULT_FAIL);
				printf("\n%s\n", details);
				pthread_exit(NULL);
			}
			if(!st.st_mode){
				//means open file failed
				noSendNum++;
				error_to_exit(&operation, &mysql, details, ";can not open!process failed!", RESULT_FAIL);
				printf("\n%s\n", details);
				pthread_exit(NULL);
			}
			//to judge if this file was changed
			int isFileExist = -1;//if file exist, then isFileExist = 0.
	//-----------------------------------------------------------------is_file_changed(MYSQL *mysql, operate_struct *operation, char error_info[4000]): if file had chenged, return 1; else, return 0 
			if(is_file_changed(&mysql, &operation.op_file, &isFileExist, mysql_addr, mysql_user_name, mysql_password, mysql_db_name)){
				// The file is changed, send file to Server
	//-------------------------------------------------------------------send_file(): if success, return 0;else return -1.
				//send file information first, to judge noSendNum++;
					//generate message to be sent to server
				char message[MAXTRANSLENGTH];
				//gen_send_massege(message, &operation, process_index - noSendNum, 0);
				//if(sendto(sock_id, message, strlen(message), 0, (struct sockaddr *)&recver_addr, sizeof(recver_addr)) < 0){
					//send message failed.
				//	noSendNum++;
					//error_to_exit(&operation, &mysql, details, "error when try to send CREATE or CLOSE_WRITE or MOVED_TO or MODIFY or Attrib operate of dir to Server.", RESULT_FAIL);
				//	printf("dir: %s ----------Failed!\n", operation.op_file.path);
				//	pthread_exit(NULL);
				//}
				time(&start);
				if(send_file(sock_id, &operation, recver_addr, sizeof(struct sockaddr_in), details, process_index - noSendNum, inotifyType)){
					//the operation of sending file to Server is failed
					printf("Failed!\n");
					time(&operation.op_end_time);
					if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
						error_to_exit(&operation, &mysql, details, ";Error when try to insert operation information!", RESULT_FAIL);
						pthread_exit(NULL);
					}
				}
				time(&end);
				printf("Success!\n");
				printf("send file time %ld\n", end - start);
				time(&operation.op_end_time);
				strcpy(operation.op_result, RESULT_SUCCESS);
				if(!isFileExist){
					//try to update the information of this file in mysql
					if(op_sql_file_list(&mysql, NULL, MYSQL_UPDATE, &operation.op_file, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
						error_to_exit(&operation, &mysql, details, ";Error when try to update information of file s information!", RESULT_SUCCESS);
						printf("\n%s\n", details);
						pthread_exit(NULL);
					}
				}else{
					//try to insert the information of this file in mysql
					if(op_sql_file_list(&mysql, NULL, MYSQL_INSERT, &operation.op_file, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
						error_to_exit(&operation, &mysql, details, ";Error when try to insert information of file s information!", RESULT_SUCCESS);
						printf("\n%s\n", details);
						pthread_exit(NULL);
					}
				}
				//try to add the information about this operation into log(mysql)
				if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
					error_to_exit(&operation, &mysql, details, ";Error when try to insert operation information of file's no change to log.", RESULT_SUCCESS);
					printf("\n%s\n", details);
					pthread_exit(NULL);
				}
			}else{
				//file no change, don't send file
				printf("Success! no Send Num Plus\n");
				noSendNum++;
				
				time(&operation.op_end_time);
				strcpy(operation.op_result, RESULT_SUCCESS);
				if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
					error_to_exit(&operation, &mysql, details, ";Error when try to insert operation information of file's no change to log.", RESULT_SUCCESS);
					printf("\n%s\n", details);
					pthread_exit(NULL);
				}
			}
		}
	}else if((strstr(operation.op_events, events[DELETE]) != NULL) || (strstr(operation.op_events, events[MOVED_FROM]) != NULL)){
		char message[MAXTRANSLENGTH];
		gen_send_massege(message, &operation, process_index - noSendNum, 0);
		printf("time: %ld, send Delete or Moved_from file %s info to receiver ---------------", operation.op_start_time, operation.op_file.path);
		if(sendto(sock_id, message, strlen(message), 0, (struct sockaddr *)&recver_addr, sizeof(recver_addr)) < 0){
			noSendNum++;
			//error_to_exit(&operation, &mysql, details, ";error when try to send DELETE or MOVED_FROM operate of file to receiver.", RESULT_FAIL);
			printf("Failed! %s\n", details);
			pthread_exit(NULL);
		}
		if(op_sql_file_list(&mysql, NULL, MYSQL_DELETE, &operation.op_file, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
			error_to_exit(&operation, &mysql, details, ";error when try to delete file s information!", RESULT_FAIL);
			printf("success! But mysql operation failed!\n");
			pthread_exit(NULL);
		}
		//将相关操作信息写入日志
		printf("Success!\n");
		
		strcpy(operation.op_result, RESULT_SUCCESS);
		time(&operation.op_end_time);
		if(op_sql_log(&mysql, &operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
			error_to_exit(&operation, &mysql, details, ";Error when try to insert delete operation information to log.", RESULT_SUCCESS);
			printf("\n%s\n", details);
			pthread_exit(NULL);
		}
	}
}

void *process_events(){
	int i = 0;
	int flag;
	void *return_info;
	while(1){
		pthread_mutex_lock(&events_lock);
			//the return values of pthread_kill includes: success(0);thread not exist(ESRCH);singnal illegal(EINVAL)
		if(event_list != NULL && ((process_thread != 0 && ((int)pthread_kill(process_thread, 0) != 0)) || process_thread == 0)){
			//if there are events in the event_list, and the process_thread is not raised or is finished
				//get the information of event
			char *event = event_list->event_info;
			pthread_mutex_unlock(&events_lock);
			if(strlen(event)){
				//if the event info is not empty
				printf("the #%d event information: %s\n", process_index, event);
				char *information = strdup(event);
				
				//split the information into three fields: event time, operation type and file name
				argv[EVENTTIMEINDEX] = information;
				if(argv[EVENTTIMEINDEX] && strlen(argv[EVENTTIMEINDEX])){
					argv[EVENTSINDEX] = strchr(argv[EVENTTIMEINDEX], ' ');
					if(argv[EVENTSINDEX] != NULL)
						*argv[EVENTSINDEX]++ = '\0';
				}
				if(argv[EVENTSINDEX] && strlen(argv[EVENTSINDEX])){
					argv[FILENAMEINDEX] = strchr(argv[EVENTSINDEX], ' ');
					if(argv[FILENAMEINDEX] != NULL){
						*argv[FILENAMEINDEX]++ = '\0';
						int length = strlen(argv[FILENAMEINDEX]);
						if(*(argv[FILENAMEINDEX] + length - 1) == '/')
							*(argv[FILENAMEINDEX] + length - 1) = '\0';
					}
					//printf("file name = %s\n", argv[FILENAMEINDEX]);
				}
				//clear the variable of  process_thread
				memset(&process_thread, 0, sizeof(process_thread));
				printf("start to process\n");
				//raise the process thread: function named process_event
				if(((int)pthread_create(&process_thread, NULL, process_event, NULL) == 0)){
					printf("create thread success!\n");
					pthread_join(process_thread, return_info);
					printf("process thread end!\n");
				}
				free(information);
			}else{
				printf("the number %dth events info is null\n", process_index);
				noSendNum++;
			}
			pthread_mutex_lock(&events_lock);
			Event_node *curNode = event_list;
			event_list = event_list->next;
			if(event_list == NULL){
				tail = NULL;
			}
			//free the event node generated in main
			free(curNode);
			process_index++;
		}
		pthread_mutex_unlock(&events_lock);
	}
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	printf("Welcome use sigsync!\n");
	if(argc > 1 && !strcmp(argv[1], "daemon")){
		//call the reveiver function
		int result = receiver_main();
		printf("Thanks, bye\n");
	}
	//this is sender function
		//get the catagory to be watched
	GetProfileString("./sigsync.conf", "client", "catalog", catalog);
		//this parameter represents the location of inotify
	GetProfileString("./sigsync.conf", "client", "inotifyPath", inotifyPath);
	//initial some params
		//mysql parameters
	GetProfileString("./sigsync.conf", "mysql", "addr", mysql_addr);
	GetProfileString("./sigsync.conf", "mysql", "name", mysql_user_name);
	GetProfileString("./sigsync.conf", "mysql", "password", mysql_password);
	GetProfileString("./sigsync.conf", "mysql", "db_name", mysql_db_name);
		//server parameters
	GetProfileString("./sigsync.conf", "server", "ip", recver_ip);
	GetProfileString("./sigsync.conf", "server", "port", recver_port_string);
	GetProfileString("./sigsync.conf", "server", "inotifyType", inotifyTypeString);
		//generate the comand of inotify
	char cmd[MAXFILEPATHLENGTH];
	char *temp_start = "inotifywait -mrq --timefmt '%d/%m/%y-%H:%M' --format '%T %e %w%f' -e modify,create,delete,attrib,close_write,move ";
	strcat(cmd, inotifyPath);
	strcat(cmd, temp_start);
	strcat(cmd, catalog);

	char *type = "r";
		//get the input popen of inotify
	FILE *info = popen(cmd, type);
		//to save the input information from inotify
	char infocontent[MAXEVENTSLENGTH];
	process_index = 0;

	//build socket，and save sock_id
	if((sock_id = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
		//write the error information into the mysql
		printf("error creating sicket\n");
		error_to_exit(&operation, &mysql, details, ";Error Creating Socket.", RESULT_FAIL);
		pthread_exit(NULL);
	}
	//init socket
	bzero(&recver_addr, sizeof(recver_addr));
	recver_addr.sin_family = AF_INET;
	recver_addr.sin_port = htons(atoi(recver_port_string));
	inet_pton(AF_INET, recver_ip, &recver_addr.sin_addr);
	inotifyType = atoi(inotifyTypeString);
	//numInfo = 0;
	//cur_index = 0;
	//int p_id = 0;
	int eventNodeSize = sizeof(Event_node);
	//init events_lock: the lock of event
	pthread_mutex_init (&events_lock,NULL);
	//raise the main thread: function named process_events
	pthread_create(&main_thread, NULL, process_events, NULL);
	while(1){
		//try to get information from the popen of inotify
		if(fgets(infocontent, MAXEVENTSLENGTH, info) != NULL){
			printf("%s--------------------------------------------------\n", infocontent);
			//the last character of infocontent is '\n', change it into '\0'
			infocontent[(int)strlen(infocontent) - 1] = '\0';
			//malloc a new space to handle this new event, we must free this space later
			curEvent = (Event_node *)malloc(eventNodeSize);
			//clear this space
			memset(curEvent, 0, eventNodeSize);
			//copy the infocontent into the event_info field
			strcpy(curEvent->event_info, infocontent);
			curEvent->next = NULL;
			//lock the events_lock
			pthread_mutex_lock(&events_lock);
			//add this new event into the list of events: event_list
			if(event_list == NULL){
				tail = curEvent;
				event_list = curEvent;
			}else{
				tail->next = curEvent;
				tail = tail->next;
			}
			//unlock the events_lock
			pthread_mutex_unlock(&events_lock);
			//clear the infocontent and wait for new events
			memset(infocontent, 0, EVENTLENGTH);
		}
	}
	printf("Good Bye!\n");
	return 0;
}
