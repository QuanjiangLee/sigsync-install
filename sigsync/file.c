#include "sigsync.h"

#define ReSendSize 100
#define NumPackets 10
#define SleepTime 3

int is_file_changed(MYSQL *mysql, struct file_struct *file, int *isFileExist, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name){
	//尝试从数据库中找到对应的文件信息，若找到，进行下一步判定；否则，返回0
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql[MAXSQLLENGTH];
	bzero(sql, MAXSQLLENGTH);
	sprintf(sql, "Select checksum, actual_size from sig_file_list where path = \'%s\';", strlen(file->path)? file->path : "NULL");
	if(!exct_query(mysql, &res, MYSQL_SELECT, sql, mysql_addr, mysql_user_name, mysql_password, mysql_db_name)){
		//查找成功则判断文件是否改变
		if(res->row_count){
			*isFileExist = 0;
			row = mysql_fetch_row(res);
			char *checksum = row[0];
			char *size = row[1];
			if(!strcmp(checksum, file->checksum) && (file->actual_size == atoi(size))){
				//means no change
				return 0;
			}
		}
	}
	//means file changed
	return 1;
}

int send_file(int sock_id, struct operate_struct *operation, struct sockaddr_in serv_addr, int serv_addr_len, char *details, int operateNum,  int inotifyType){
	FILE *fp;
	char read[MAXREADLENGTH];
	char massege[MAXTRANSLENGTH];
	char checksum[BASE64CHECKSUM];
	int send_len;
	int read_len;
	long int total_send_length = 0;
	
	char operateString[10];
	int operateNumLength;
	char dataNumString[10];
	int dataNumStringLength;
	char countString[10];
	int countStringLength;
	int extLength = 0;

	char backup[ReSendSize][MAXTRANSLENGTH];
	int send_length[ReSendSize];
	int send_count = 0;

	sprintf(operateString, "%d", operateNum);
	operateNumLength = (int)strlen(operateString);

	int dataNum = 0;
	if(operation->op_file.actual_size % MAXREADLENGTH){
		dataNum = operation->op_file.actual_size / MAXREADLENGTH + 1;
	}else{
		dataNum = operation->op_file.actual_size / MAXREADLENGTH;
	}
	sprintf(dataNumString, "%d", dataNum);
	dataNumStringLength = (int)strlen(dataNumString);
		//open file
    	if((fp = fopen(operation->op_file.path, "r")) == NULL){
            return -1;
    	}
		//send file information
	long int start, end;
	//sleep(2);
	gen_send_massege(massege, operation, operateNum, dataNum);
	if(sendto(sock_id, massege, strlen(massege), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
		time(&operation->op_end_time);
		strcpy(operation->op_result, "FAIL");
		strcat(details, "error when try to send operation information of file to receiver.");
		return -1;
	}
        //read and send
	bzero(read, MAXREADLENGTH);
    	bzero(massege, MAXTRANSLENGTH);
	bzero(checksum, BASE64CHECKSUM);
	int count = 1;
	int total_sendNum = 2;
	time(&start);
   	while((read_len = fread(read, sizeof(char), MAXREADLENGTH, fp)) > 0){
		char tempContent[MAXTRANSLENGTH];
		//当从文件中读取数据之后，需要对数据计算相关的校验值
		strncopy(tempContent, read, read_len);
		if(get_md5_checksum(tempContent, checksum, details, read_len)){
			printf("send file error becouse of checksum is failed\n");
			strcat(details, "get checksum of content failed!From send_file in file.c.");
        		sendto(sock_id, "trans failed!", strlen("trans failed!"), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
			return -1;
		}
		sprintf(massege, "%d%s%d%s%s%s%d%s", operateNum, SPLITSTRING, dataNum, SPLITSTRING, checksum, SPLITSTRING, count, SPLITSTRING);
		my_strncat(massege, read, read_len);
		sprintf(countString, "%d", count);
		countStringLength = (int)strlen(countString);
		extLength = operateNumLength + dataNumStringLength + countStringLength + 36;
		if(count % inotifyType == 0)
			usleep(1);
        	send_len = sendto(sock_id, massege, read_len + extLength, 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
		//strncopy(backup[send_count], massege, read_len + extLength);
		//send_length[send_count++] = read_len + extLength;
		//usleep(100);
		total_sendNum++;
		total_send_length += read_len;
        	if(send_len < read_len + extLength){
        		printf("Send file failed!\n");
			//出错处理
			strcat(details, "transport file failed!From send_file in file.c.");
			return -1;
        	}
		count++;
		if(total_send_length >= operation->op_file.actual_size)
			break;
        	bzero(read, MAXREADLENGTH);
		//bzero(buf, MAXTRANSLENGTH);
		bzero(checksum, BASE64CHECKSUM);
		usleep(100);
    }
	time(&end);
	//close file pointer
	printf("\n\ttrans over!\ttotal size: %ld; count = %d, total_send_n = %d, time = %ld\n", total_send_length, count, total_sendNum, end - start);
	fclose(fp);
	return 0;
}

char *get_old_name(const char *fname){
	char *flag = "-old";
	char *result = (char *)malloc(strlen(fname) + strlen(flag) + 1);
	if(result == NULL){
		//printf("out of memory from get_name_old.\n");
		return NULL;
	}
		//save the start pointer of result
	char *temp = result;
	char *file_name = strdup(fname);
	while( *file_name != '\0'){
                *result++ = *file_name++;
        }
	file_name = flag;
	while((*result++ = *file_name++) != '\0');
	return temp;
}

void change_old_file_name(const char *fname){
	char *old_name = get_old_name(fname);
	if(!access(old_name, 0)){
		//printf("file %s is exsit, remove ", old_name);
		//if(!remove(old_name))
		remove(old_name);
			//printf("success REMOVE OLD FILE!\n");
		//else
			//printf("failed!\n");
	}
	if(!access(fname, 0)){
		//printf("file %s is exsit, rename %s to %s ", fname, fname, old_name);
		//if(!rename(fname, old_name))
		rename(fname, old_name);
			//printf("RENAME FILE success!\n");
		//else
			//printf("failed");
	}
	free(old_name);
}


int generate_file(struct operate_struct *operation, char ***trans_data, int **trans_data_length, int cur_operate_index, int cur_transbuf_index, int numData, char *details){
	int recv_len, total_write = 0;
	int lost = 0;
	//char buf[MAXTRANSLENGTH];
	int flag;
	FILE *fp;
		//try to open file of name $fname
	if((fp = fopen(operation->op_file.path, "w")) == NULL){
		strcat(details, "Open file failed!");
		printf("Open file failed!\n");
		return -1;
	}
	//bzero(buf, MAXTRANSLENGTH);
	int operateIndex = cur_operate_index % MAXOPERATE;
	while(1){
		//printf("cur_transbuf_index:%d numData:%d\n", cur_transbuf_index, numData);
		while(cur_transbuf_index <= numData && trans_data[operateIndex][cur_transbuf_index]){
			int len = trans_data_length[operateIndex][cur_transbuf_index];
			//printf("from generate file function: %d, numData[%d]=%d\n", cur_transbuf_index, operateIndex, numData[operateIndex]);
			//printf("len = %d,", len);
			//strncopy(buf, trans_data[operateIndex][cur_transbuf_index], len);
			int write_len = fwrite(trans_data[operateIndex][cur_transbuf_index], sizeof(char), len, fp);
			cur_transbuf_index++;
			if(write_len != len){
				//strcat(details, "File write failed!");
				printf("file write failed!\n");
				fclose(fp);
				return -1;
			}
			total_write += len;
			//printf("total_write=%d,this time=%d,index=%d\n", total_write, len, cur_transbuf_index - 1);
			if(total_write  >= operation->op_file.actual_size){
				//printf("\n\tfile write finished!total_write = %d\n", total_write);
				//strcat(details, "file write finished!wait for trans finish flag.");
				fclose(fp);
				return 0;
			}
			//bzero(buf, MAXTRANSLENGTH);
		}
		sleep(1);
		if(cur_transbuf_index < numData && trans_data[operateIndex][cur_transbuf_index]){
			//printf("continue to write in.\n");
			continue;
		}else if(cur_transbuf_index < numData){
			printf("unit lost\n");
			lost++;
			cur_transbuf_index++;
			continue;
		}
		//printf("\n\tfile closed, total_write = %d, numData=%d, lost=%d\n", total_write, numData, lost);
		fclose(fp);
		break;
	}
	return 0;
}

int create_new_file(struct operate_struct *operation, char ***trans_data, int **trans_data_length, 
		int cur_operate_index, int cur_transbuf_index, int numData, int old_version, char *details){
	//printf("Start to create new file: %s\n", operation->op_file.path);
	char info[MAXTRANSLENGTH];
	if(old_version){
		if(!access(operation->op_file.path, 0)){
			change_old_file_name(operation->op_file.path);
		}
	}
	return generate_file(operation, trans_data, trans_data_length, cur_operate_index, cur_transbuf_index, numData, details);
}

int isGenerateFile(struct operate_struct *operation, MYSQL *mysql, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH], int *isFileExist){
	MYSQL_RES *res;
	MYSQL_ROW row;
	if(op_sql_file_list(mysql, &res, MYSQL_SELECT, &operation->op_file, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details) != 0){
		process_error(mysql, operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, "Error when try to select file information from mysql!\n");
		return -1;
	}
	if(!res || (res->row_count == 0)){
		*isFileExist = 0;
		return 0;
	}else{
		*isFileExist = 1;
		row = mysql_fetch_row(res);
		if(!strcmp(operation->op_file.checksum, row[0]) && (operation->op_file.actual_size == atoi(row[1])))
			return -1;
		else
			return 0;
	}
}
