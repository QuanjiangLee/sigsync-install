/* 标准输入输出流 */
#include <stdio.h>
/*  stdlib.h意思是standard library。 */
#include <stdlib.h>
/* 包含字符串处理的头文件  */
#include <string.h>
/*  unistd.h 不是c语言的东西，是linux/unix的系统调用，包含了许多unix系统服务的函数原型。如read、write和getpid函数。
**  提供了通用的文件、目录、程序以及进程操作的函数。
*/
#include <unistd.h>
/* include some types of system */
#include <sys/types.h>
/* 定义了许多宏和open、fcntl函数原型，提供了对文件控制的函数 */
#include <fcntl.h>
/* 提供socket函数和数据结构 */
#include <sys/socket.h>
/* 定义数据结构：sockaddr_in */
#include <netinet/in.h>
/* 提供ip地址转换函数 */
#include <arpa/inet.h>
/**/
#include <sys/stat.h>
/**/
#include <sys/mman.h>
/**/
#include <mysql/mysql.h>
/**/
#include <string.h>
/*提供有关时间的函数*/
#include <time.h>
/*提供错误号errno的定义，用于错误处理*/
#include <errno.h>
/*提供多线程操作的函数*/
#include <pthread.h>
/**/
#include <assert.h>
/*
**netdb.h	提供设置及获取域名的函数
**sys/ioctl.h	提供对i/o控制的函数
**sys/poll.h	提供socket等待测试机制的函数
**crypt.h	提供使用DES加密算法的加密函数	添加之后在编译时需要
**pwd.h		提供对/etc/passwd文件访问的函数
**shadow.h	提供对/etc/shadow文件访问的函数
**signal.h	提供对信号操作的函数
**sys/wait.h、sys/ipc.h、sys/shm.h提供进程等待、进程间通讯(IPC)以及共享内存的函数
*/
/* block size to write into file */
//这里实现的是对机器型号的适应，但是编译通不过
/*
#ifndef int32
#if (SIZEOF_INT == 4)
#define int32 int
#elif (SIZEOF_LONG == 4)
#define int32 long
#elif (SIZEOF_SHORT == 4)
#define int32 short
#endif
#define int32 int
#endif
*/
#define int32 int

#ifndef uchar
#define uchar unsigned char
#endif

#ifndef uint32
#define uint32 unsigned int32
#endif

#ifndef PROTOTYPES 
#define PROTOTYPES 0 
#endif 
 
/* POINTER defines a generic pointer type */ 
typedef unsigned char *POINTER; 
 
/* UINT2 defines a two byte word */ 
typedef unsigned short int UINT2; 
 
/* UINT4 defines a four byte word */ 
typedef unsigned long int UINT4; 
 
/* PROTO_LIST is defined depending on how PROTOTYPES is defined above. 
If using PROTOTYPES, then PROTO_LIST returns the list, otherwise it 
  returns an empty list. 
 */ 
#if PROTOTYPES 
#define PROTO_LIST(list) list 
#else 
#define PROTO_LIST(list) () 
#endif

#define CHECKSUMLENGTH 16
#define MAXREADLENGTH 1000
#define MAXTRANSLENGTH 1066
#define MAXUPDATEINFO 1000
#define MAXSQLLENGTH 4000
#define NUMMASSEGEPARAMS 21

#define GENERATELENGTH 100
#define MAXFILEPATHLENGTH 4000
#define EVENTLENGTH 32
#define MAXINFORMATION 1000
#define BASE64CHECKSUM (CHECKSUMLENGTH * 2 + 1)
#define OWNERNAMELENGTH 64
#define IPADDRSLENGTH 128
#define RESULTLENGTH 5
#define MAXREMARKS 4000

#define MAXOPERATE 1000
#define MAXTRANSBUFFER 1000

//用于区分操作数据包和数据数据包
#define OPERATION "0"
#define DATA "7"
//用于将不同的信息之间进行区分的符号
#define SPLITSTRING "@"
#define SPLITCHAR '@'

#define bzero(buf,n) memset(buf,0,n)

//之前打算添加的更新信息，暂时不用
/*
struct update_info{
	char *file_path;
	char *owner_name;
	char *file_checksum;
	long file_size;
	char *file_events;
	int file_mode;
	long atime;
	long ctime;
	long mtime;
	int type;
	char *remark;
};*/

struct file_struct{
	char path[MAXFILEPATHLENGTH];
	char path_hash[BASE64CHECKSUM];
	char owner_name[OWNERNAMELENGTH];
	char name[GENERATELENGTH];
	int parent;
	int type;
	long actual_size;
	long size_on_disk;
	long atime;
	long mtime;
	long ctime;
	char checksum[BASE64CHECKSUM];
	int mode;
	char remarks[MAXREMARKS];
};

struct operate_struct{
	struct file_struct op_file;
	char op_events[EVENTLENGTH];
	long op_start_time;
	long op_end_time;
	char op_information[MAXINFORMATION];
	char op_result[RESULTLENGTH];
	char op_sender_addr[IPADDRSLENGTH];
	char op_recver_addr[IPADDRSLENGTH];
	char op_remarks[MAXREMARKS];
};

//#include "byteorder.h"
#include "receiver.h"
#include "event.h"
//#include "main.h"
#include "md5.h"
#include "checksum.h"
#include "util.h"
#include "mysql.h"
#include "file.h"

static char mysql_addr[GENERATELENGTH];
static char mysql_user_name[GENERATELENGTH];
static char mysql_password[GENERATELENGTH];
static char mysql_db_name[GENERATELENGTH];

static char sender_ip[GENERATELENGTH];// = "192.168.184.133";
static char recver_ip[GENERATELENGTH];// = "202.117.10.187";
static char sender_port_string[][GENERATELENGTH];// = "8888";
static char recver_port_string[GENERATELENGTH];// = "8888";
static char receiverNumString[GENERATELENGTH];
static char processerNumString[GENERATELENGTH];
static char inotifyTypeString[GENERATELENGTH];

static char catalog[MAXFILEPATHLENGTH];
static char inotifyPath[MAXFILEPATHLENGTH];

//用于获得socket的id
int			sock_id;
//相关信息缓存
char details[MAXTRANSLENGTH];
