#!/bin/bash
#set -e

#binlog文件接收目录
SqlFilePath="/sigsync/"

#数据库用户
DBuser="root"

#数据库密码
DBpassword="sigsyncDb"

#数据库名
DBname="myDatabase"

tmpDblogs="/tmp/syncDBlogs/"
storeSqlFiles="/tmp/sqlFiles/"
#进入接收同步目录并创建binlog同步目录

if [ ! -d $SqlFilePath ]; then
mkdir -p $SqlFilePath
fi

if [ ! -d $tmpDblogs ]; then
mkdir -p $tmpDblogs
fi

if [ ! -d $storeSqlFiles ]; then
mkdir -p $storeSqlFiles
fi
#将所有需要同步的sql文件放在$tmpDblogs下
cd $SqlFilePath
files=$(ls $DBname*.sql 2>/dev/null | wc -l)
if [ $files != '0' ]; then
    pwd
    cp $DBname*.sql $storeSqlFiles && mv $DBname*.sql $tmpDblogs
else 
    echo "没有可同步的sql文件！"
    exit 1
fi

cd $tmpDblogs
touch error.log

#对binlog文件进行同步操作
for logName in $( ls *.sql | sort )
do
echo "sync file $logName..."
mysql --user=$DBuser --password=$DBpassword $DBname < $logName >/dev/null 2>> error.log
#同步检测，若同步完成则删除已同步的binlog文件
if [ $? -eq 0 ];then
rm -rf $tmpDblogs$logName
    echo "同步成功！"
else 
    echo "同步错误，请检查错误！"
fi
done
