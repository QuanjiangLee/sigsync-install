
#define START_PATH "data/"
char *map_file(int fd, off_t size);
void unmap_file(char *buf, off_t size);
void strncopy(char *dest, const char *src, int n);
void my_strncat(char *dest, const char *src, int n);
//void add_op_info(char *des_info, char *info);
int process_error(MYSQL *mysql, struct operate_struct *operation, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char error_info[MAXTRANSLENGTH]);
char * strsfi(char *str_short, char *str_long);
char * get_owner_name(char path[MAXFILEPATHLENGTH], char details[MAXTRANSLENGTH]);
char * get_name(char *path, char details[MAXTRANSLENGTH]);
int get_parent(MYSQL *mysql, char *path, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH]);
int get_file_type(MYSQL *mysql, char *path, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH]);
int process_massege(struct operate_struct *operation, char *buf, char *error_information);
char *gen_send_massege(char *massege, struct operate_struct *operation, int operateNum, int dataNum);
void base64(char hash[33], unsigned char bindata[CHECKSUMLENGTH]);
char * l_trim(char * szOutput, const char *szInput);
char *r_trim(char *szOutput, const char *szInput);
char * a_trim(char * szOutput, const char * szInput);
int GetProfileString(char *profile, char *AppName, char *KeyName, char *KeyVal );
