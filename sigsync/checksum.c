#include "sigsync.h"

//a simple checksum of string
uint32 get_simple_checksum(char *buf, int len){
	int i;
    uint32 s1, s2;

    s1 = s2 = 0;
    for (i = 0; i < len; i++) {
	s1 += buf[i];
	s2 += s1;
    }
    return (s1 & 0xffff) + (s2 << 16);
}

int get_md5_checksum(char *buf, char *checksum, char details[MAXTRANSLENGTH], unsigned int len){
	unsigned char digest[CHECKSUMLENGTH];
	if(MDString(buf, digest, len)){
		base64(checksum, digest);
		return 0;
	}
	//strcat(details, "error  when try to get the checksum of buf");
	return -1;
}

int get_file_checksum(char *file_name, char *sum, char details[MAXTRANSLENGTH], long int current_size){
	char *buf;
	int fd;
	bzero(sum, BASE64CHECKSUM);
	unsigned char digest[CHECKSUMLENGTH];
	if(MDFile(file_name, digest, current_size)){
		return -1;
	}
	base64(sum, digest);
	return 0;
}
