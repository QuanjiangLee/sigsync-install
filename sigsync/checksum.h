
#ifndef MAXTRANSLENGTH
#define MAXTRANSLENGTH 4000
#endif

uint32 get_simple_checksum(char *buf, int len);
int get_md5_checksum(char *fname, char *checksum, char details[MAXTRANSLENGTH], unsigned int len);
int get_file_checksum(char *file_name, char *sum, char details[MAXTRANSLENGTH], long int current_size);
