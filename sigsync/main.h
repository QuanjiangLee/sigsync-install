#define EVENTTIMEINDEX 		0
#define EVENTSINDEX 		1
#define FILENAMEINDEX 		2

#define MAXEVENTS 1000
#define MAXEVENTSLENGTH 1000
#define ARGC 3
#define GENERATELENGTH 100

typedef struct Event_node{
	char event_info[MAXEVENTSLENGTH];
	struct Event_node *next;
}Event_node, *Event_list;

char *SPACE = " ";
//char event_list[MAXEVENTS][MAXEVENTSLENGTH];
Event_list event_list;
Event_node *curEvent, *tail;
char *argv[ARGC];
int process_index;
//int cur_index;
//int numInfo;
pthread_t main_thread;
pthread_t process_thread;
pthread_mutex_t mut;
char *RESULT_SUCCESS = "SUCCE";
char *RESULT_FAIL = "FAIL";

