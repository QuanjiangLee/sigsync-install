#ifndef MAXTRANSLENGTH
#define MAXTRANSLENGTH 4000
#endif
#ifndef MAXOPERATE
#define MAXOPERATE 1000
#endif
#ifndef MAXTRANSBUFFER
#define MAXTRANSBUFFER 1000
#endif

int is_file_changed(MYSQL *mysql, struct file_struct *file, int *isFileExist, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name);
int send_file(int sock_id, struct operate_struct *operation, struct sockaddr_in serv_addr, int serv_addr_len, char *error_information, int operateNum, int inotifyType);
char *get_old_name(const char *fname);
void change_old_file_name(const char *fname);
int create_new_file(struct operate_struct *operation, char ***trans_data, int **trans_data_length, int cur_operate_index, int cur_transbuf_index, int numData, int old_version, char *details);
int generate_file(struct operate_struct *operation, char ***trans_data, int **trans_data_length, int cur_operate_index, int cur_transbuf_index, int numData, char *details);
int isGenerateFile(struct operate_struct *operation, MYSQL *mysql, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH], int *isFileExist);
