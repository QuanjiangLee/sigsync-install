#include "sigsync.h"

#define START_PATH "data/"
#define KEYVALLEN 100

char *map_file(int fd, off_t size){
	return (char *)mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
}

void unmap_file(char *buf, off_t size){
	if(size > 0)
		munmap(buf, size);
}

void strncopy(char *dest, const char *src, int n){
	assert(dest && src);
	while(n--){
		*dest++ = *src++;
	}
}

void my_strncat(char *dest, const char *src, int n){
	int i = 0;
	int base_index = (int)strlen(dest);
	for(; i < n; i++)
		dest[base_index + i] = src[i];
}

int process_error(MYSQL *mysql, struct operate_struct *operation, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char error_info[MAXTRANSLENGTH]){
	//printf("process error: %s\n", error_info);
	char *fail = "FAIL";
	strcat(operation->op_information, error_info);
	strcat(operation->op_result, fail);
	op_sql_log(mysql, operation, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, error_info);
}

char * strsfi(char *str_short, char *str_long){
	int short_len = strlen(str_short);
	int long_len = strlen(str_long);
	int first_index;
	if(short_len > long_len)
		return NULL;
	for(first_index = 0; first_index < long_len - short_len; first_index++ ){
		if(!strncmp(str_long + first_index, str_short, short_len)){
			return (str_long + first_index);
		}
	}
	return NULL;
}

char * get_owner_name(char path[MAXFILEPATHLENGTH], char details[MAXTRANSLENGTH]){
	//printf("try to get owner_name from path: %s\n", path);
	char owner_name[MAXFILEPATHLENGTH];
	char *pointer;
	//owner_name = (char *)malloc(sizeof(char) * strlen(path));
	strcpy(owner_name, path);
	//printf("owner_name = %s\n", owner_name);
	char *start_path = "data/";
	pointer = strsfi(start_path, owner_name);
	if(pointer == NULL)
		return NULL;
	pointer = strchr(pointer, '/') + 1;
	if(pointer == NULL)
		return NULL;
	int i, len = strlen(pointer);
	for(i = 0; i < len; i++){
		if(*(pointer + i) == '/'){
			*(pointer + i) = '\0';
			break;
		}
	}
	//printf("owner_name: %s\n", owner_name);
	//strcat(details, "Get owner name success!");
	return pointer;
}

char * get_name(char *path, char details[MAXTRANSLENGTH]){
	char *name = strrchr(path, '/') + 1;
	//printf("name = %s\n", name);
	//strcat(details, "Get name success!");
	return name;
}

int get_parent(MYSQL *mysql, char *path, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH]){
	char sql[MAXSQLLENGTH];
	MYSQL_RES *res;
	MYSQL_ROW row;
	int parent = -1000;
	char *parent_path = strdup(path);
	char *name = strrchr(parent_path, '/');
	if(name != NULL){
		*name = '\0';
		//printf("parent_path : %s\n", parent_path);

		bzero(sql, MAXSQLLENGTH);
		sprintf(sql, "select fileid from sig_file_list where path=\'%s\';", parent_path);	
		int result = exct_query(mysql, &res, MYSQL_SELECT, sql, mysql_addr, mysql_user_name, mysql_password, mysql_db_name);
		if(!result && res){
			//printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows (res));
			if(1 == (unsigned long)mysql_num_rows (res)){
				row = mysql_fetch_row(res);
				char *value = row[0];
				parent = strtol(value, NULL, 10);
			}else{
				parent = 0;
				//printf("there are several parents or no such parent for this file path.\n");
			}
			mysql_free_result(res);
		}else{
			parent = 0;
		}
	}else{
		parent = 0;
	}
	//mysql_close(mysql);
	//printf("parent id = %d\n", parent);
	//strcat(details, "Get parent success!");
	return parent;
}

int get_file_type(MYSQL *mysql, char *path, char *mysql_addr, char *mysql_user_name, char *mysql_password, char *mysql_db_name, char details[MAXTRANSLENGTH]){
	char sql[MAXSQLLENGTH];
	MYSQL_RES *res;
	MYSQL_ROW row;
	int type = -1;
	char *typeString = strrchr(path, '.');
	//printf("type string: %s\n", typeString);

	//bzero(sql, MAXSQLLENGTH);
	//sprintf(sql, "select type from sig_file_list where path='%s';", path);
	//exct_query(mysql, &res, MYSQL_SELECT, sql);
	//if(res){
		//printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows (res));
	//	if(1 == (unsigned long)mysql_num_rows (res)){
	//		row = mysql_fetch_row(res);
	//		char *value = row[0];
	//		type = strtol(value, NULL, 10);
	//	}
	//}
	//mysql_free_result(res);
	//printf("type = %d\n", type);
	//strcat(details, "Get type success!");
	return type;
}

int process_massege(struct operate_struct *operation, char *buf, char *details){
	char *param = strdup(buf);
	char *tok;
	char *params[NUMMASSEGEPARAMS];
	int numParams = 0;
	struct file_struct *file = &operation->op_file;
	//printf("initial params : %s\n", buf);
	if(strlen(param) == 0){
		//printf("buf is null. -%s-\n", buf);
		return -1;
	}
	
	for(tok = strtok(param,";") ; tok ; tok = strtok(NULL, ";")){
		params[numParams++] = tok;
	}

	bzero(operation, sizeof(*operation));
	bzero(file, sizeof(struct file_struct));
	//为参数赋值
	//printf("params start\n");
	char *value = params[0];
	if(strcmp(value, "No Path"))
		strcpy(file->path, value);
	else{
		//printf("Path of file is null in the massege. path = %s\n", value);
		strcat(details, "Path of file is null in the massege.");
		return -1;
	}
	//printf("%s\n", value);
	value = params[1];
	if(strcmp(value, "No Hash"))
		strcpy(file->path_hash, value);
	else{
		get_md5_checksum(file->path, file->path_hash, details, strlen(file->path));
	}
	//printf("%s\n", value);
	value = params[2];
	if(!strcmp(value, "No owner"))
		bzero(file->owner_name, OWNERNAMELENGTH);
	else
		strcpy(file->owner_name, value);
	//printf("%s\n", value);
	value = params[3];
	if(!strcmp(value, "No name"))
		bzero(file->name, GENERATELENGTH);
	else
		strcpy(file->name, value);
	//printf("%s\n", value);
	value = params[4];
	if(value)
		file->parent = (int)strtol(value, NULL, 10);
	else
		file->parent = 0;
	//printf("%d\n", file->parent);
	value = params[5];
	if(value)
		file->type = (int)strtol(value, NULL, 10);
	else
		file->type = 0;
	//printf("%d\n", file->type);
	value = params[6];
	if(value)
		file->actual_size = strtol(value, NULL, 10);
	else
		file->actual_size = 0;
	//printf("%ld\n", file->actual_size);
	value = params[7];
	if(value)
		file->size_on_disk = strtol(value, NULL, 10);
	else
		file->size_on_disk = 0;
	//printf("%ld\n", file->size_on_disk);
	value = params[8];
	if(value)
		file->atime = strtol(value, NULL, 10);
	else
		file->atime = 0;
	//printf("%ld\n", file->atime);
	value = params[9];
	if(value)
		file->mtime = strtol(value, NULL, 10);
	else
		file->mtime = 0;
	//printf("%ld\n", file->mtime);
	value = params[10];
	if(value)
		file->ctime = strtol(value, NULL, 10);
	else
		file->ctime = 0;
	//printf("%ld\n", file->ctime);
	value = params[11];
	if(!strcmp(value, "No checksum"))
		strcpy(file->checksum, "");
	else
		strcpy(file->checksum, value);
	//printf("%s\n", value);
	value = params[12];
	if(value)
		file->mode = (int)strtol(value, NULL, 10);
	else
		file->mode = 0;
	//printf("%d\n", file->mode);
	value = params[13];
	if(!strcmp(value, "No remarks"))
		strcpy(file->remarks, "");
	else
		strcpy(file->remarks, value);
	//printf("%s\n", value);
	value = params[14];
	if(!strcmp(value, "No event"))
		strcpy(operation->op_events, "");
	else
		strcpy(operation->op_events, value);
	//printf("%s\n", value);
	value = params[15];
	if(value)
		operation->op_start_time = strtol(value, NULL, 10);
	else
		operation->op_start_time = 0;
	//printf("%ld\n", operation->op_start_time);
	value = params[16];
	if(value)
		operation->op_end_time = strtol(value, NULL, 10);
	else
		operation->op_end_time = 0;
	//printf("%ld\n", operation->op_end_time);
	value = params[17];
	if(!strcmp(value, "No information"))
		strcpy(operation->op_information, "");
	else
		strcpy(operation->op_information, value);
	//printf("%s\n", value);
	value = params[18];
	if(!strcmp(value, "No addr"))
		strcpy(operation->op_sender_addr, "");
	else
		strcpy(operation->op_sender_addr, value);
	//printf("%s\n", value);
	value = params[19];
	if(!strcmp(value, "No addr"))
		strcpy(operation->op_recver_addr, "");
	else
		strcpy(operation->op_recver_addr, value);
	//printf("%s\n", value);
	value = params[20];
	if(!strcmp(value, "No remarks"))
		strcpy(operation->op_remarks, "");
	else
		strcpy(operation->op_remarks, value);
	return 0;
}

char *gen_send_massege(char *massege, struct operate_struct *operation, int operateNum, int dataNum){
	struct file_struct *file = &operation->op_file;
	char content[MAXREADLENGTH];
	char hash[BASE64CHECKSUM];
	bzero(massege, MAXTRANSLENGTH);
	bzero(content, MAXREADLENGTH);
	bzero(hash, BASE64CHECKSUM);
	sprintf(content, "%s;%s;%s;%s;%d;%d;%ld;%ld;%ld;%ld;%ld;%s;%d;%s;%s;%ld;%ld;%s;%s;%s;%s",
			strlen(file->path)? file->path:"No Path", 
			strlen(file->path_hash)? file->path_hash : "No Hash", 
			strlen(file->owner_name)? file->owner_name : "No owner", 
			strlen(file->name)? file->name : "No name", 
			file->parent, 
			file->type, 
			file->actual_size, 
			file->size_on_disk, 
			file->atime, file->mtime, file->ctime, 
			strlen(file->checksum)? file->checksum : "No checksum", 
			file->mode, 
			strlen(file->remarks)? file->remarks : "No remarks",
			strlen(operation->op_events)? operation->op_events : "No event", 
			operation->op_start_time, operation->op_end_time, 
			strlen(operation->op_information)? operation->op_information : "No inforamtion", 
			strlen(operation->op_sender_addr)? operation->op_sender_addr : "No addr", 
			strlen(operation->op_recver_addr)? operation->op_recver_addr : "No addr", 
			strlen(operation->op_remarks)? operation->op_remarks : "No remarks");
	get_md5_checksum(content, hash, NULL, strlen(content));
	sprintf(massege, "%d%s%d%s%s%s0%s%s", operateNum, SPLITSTRING, dataNum, SPLITSTRING, hash, SPLITSTRING, SPLITSTRING, content);
	return massege;
}

void base64(char hash[33], unsigned char bindata[CHECKSUMLENGTH]){
	unsigned char base64[CHECKSUMLENGTH * 2];
	bzero(base64, CHECKSUMLENGTH * 2);
	int i = 0;
	//printf("checksumlength: %d\n", CHECKSUMLENGTH);
	char temp[2];
	for(i = 0; i < CHECKSUMLENGTH ; i++){
		//printf("%d: %02x\n", i, bindata[i]);
		sprintf(temp, "%02x", bindata[i]);
		strcat(base64, temp);
	}
	for(i = 0; i < CHECKSUMLENGTH * 2; i++){
		hash[i] = base64[i];
	}
	hash[32] = '\0';
}

/*   删除左边的空格   */
char * l_trim(char * szOutput, const char *szInput){
	assert(szInput != NULL);
	assert(szOutput != NULL);
	assert(szOutput != szInput);
	while(*szInput != '\0' && isspace(*szInput)){
   		++szInput;
	}
 	return strcpy(szOutput, szInput);
}

/*   删除右边的空格   */
char *r_trim(char *szOutput, const char *szInput){
	char *p = NULL;
 	assert(szInput != NULL);
 	assert(szOutput != NULL);
 	assert(szOutput != szInput);
 	strcpy(szOutput, szInput);
 	for(p = szOutput + strlen(szOutput) - 1; p >= szOutput && isspace(*p); --p){
		;
	}
	*(++p) = '\0';
	return szOutput;
}

/*   删除两边的空格   */
char * a_trim(char * szOutput, const char * szInput){
	char *p = NULL;
	assert(szInput != NULL);
	assert(szOutput != NULL);
	l_trim(szOutput, szInput);
	for(p = szOutput + strlen(szOutput) - 1;p >= szOutput && isspace(*p); --p){
		;
	}
	*(++p) = '\0';
	return szOutput;
}


int GetProfileString(char *profile, char *AppName, char *KeyName, char *KeyVal ){
	char appname[32],keyname[32];
	char *buf,*c;
	char buf_i[KEYVALLEN], buf_o[KEYVALLEN];
	FILE *fp;
	int found=0; /* 1 AppName 2 KeyName */
	if( (fp=fopen( profile,"r" ))==NULL ){
		printf( "openfile [%s] error [%s]\n",profile,strerror(errno) );
		return(-1);
	}
	fseek( fp, 0, SEEK_SET );
	memset( appname, 0, sizeof(appname) );
	sprintf( appname,"[%s]", AppName );

	while( !feof(fp) && fgets( buf_i, KEYVALLEN, fp )!=NULL ){
		l_trim(buf_o, buf_i);
		if( strlen(buf_o) <= 0 )
			continue;
		buf = NULL;
		buf = buf_o;

		if( found == 0 ){
			if( buf[0] != '[' ) {
				continue;
			} else if ( strncmp(buf,appname,strlen(appname))==0 ){
				found = 1;
				continue;
			}
		} else if( found == 1 ){
			if( buf[0] == '#' ){
				continue;
			} else if ( buf[0] == '[' ) {
				break;
			} else {
				if( (c = (char*)strchr(buf, '=')) == NULL )
					continue;
				memset( keyname, 0, sizeof(keyname) );
				sscanf( buf, "%[^=|^ |^\t]", keyname );
				if( strcmp(keyname, KeyName) == 0 ){
					sscanf( ++c, "%[^\n]", KeyVal );
					char *KeyVal_o = (char *)malloc(strlen(KeyVal) + 1);
					if(KeyVal_o != NULL){
						memset(KeyVal_o, 0, sizeof(KeyVal_o));
						a_trim(KeyVal_o, KeyVal);
						if(KeyVal_o && strlen(KeyVal_o) > 0)
							strcpy(KeyVal, KeyVal_o);
						free(KeyVal_o);
						KeyVal_o = NULL;
					}
					found = 2;
					break;
				} else {
					continue;
				}
			}
		}
	}
	fclose( fp );
	if( found == 2 )
		return(0);
	else
		return(-1);
}

/*
int main(int argc, char *argv[]){
	char *destInfo;
	//文件类型
	struct stat st;
	char *owner_name, name;
	int parent, type;
	char details[MAXTRANSLENGTH];
	struct operate_struct operation;
	struct file_struct *file = &operation.op_file;	

	MYSQL mysql;
	if(init_mysql_connect(&mysql, mysql_addr, mysql_user_name, mysql_password, mysql_db_name, details)){
		printf("%.*s\n", (int)strlen(details), details);
		printf("Please chekc the informations and try again!\n");
		return -1;
	}
	
	char *info = "This is a test string of function add_op_info";
	add_op_info(destInfo, info);

	char *path = "/sigsync/data/admin/nohup.out";
	
	file->path = path;
	char checksum[CHECKSUMLENGTH];
	get_md5_checksum(file->path, checksum, details);
	base64(file->path_hash, checksum);
	printf("%d,checksum: %s\n", (int)strlen(file->path_hash), file->path_hash);
	file->owner_name = get_owner_name(file->path, details);
	file->name = get_name(file->path, details);
	file->parent = get_parent(&mysql, file->path, details);
	operation.op_file.type = get_file_type(&mysql, operation.op_file.path, details);
	//file->actual_size = 0;
	
	if(lstat(file->path, &st)){
		printf("open file failed!If event is delete, then ok.\n");
		exit(-1);
	}else{
		//printf("%ld\n", st.st_size);
		file->actual_size = st.st_size;
		file->size_on_disk = st.st_blksize * st.st_blocks;
		file->atime = st.st_atime;
		file->mtime = st.st_mtime;
		file->ctime = st.st_ctime;
		bzero(operation.op_file.checksum, CHECKSUMLENGTH * 2 + 1);
		bzero(details, 4000);
		get_file_checksum(file->path, file->checksum, st.st_size, details);
		file->mode = st.st_mode;
		file->remarks = "file's remarks";
	}
	operation.op_events = "CREATE";
	time(&operation.op_start_time);
	operation.op_end_time = operation.op_start_time + 1000;
	operation.op_information = "operation's information";
	operation.op_result = "success";
	operation.op_sender_addr = "192.168.10.117";
	operation.op_recver_addr = "192.168.10.117";
	operation.op_remarks = "operation remarks";
	
	char massege[4000];
	gen_send_massege(massege, &operation);
	printf("massege: %s\n", massege);
	//massege = gen_send_massege(massege, &operation);
	//printf("massege: %s\n", massege);
	struct operate_struct new_operation;
	char error_info[MAXTRANSLENGTH];
	process_massege(&new_operation, massege, error_info);
}
*/
